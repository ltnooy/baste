========
Overview
========
Baste - ( tr ) to sew with loose temporary stitches.

Baste is a set of extensions intended to be used with Fabric in order to
provide a development environment for python projects. Although not strictly
necessary - it is intended to be used with virtualenv and pip. Together they
provide an simple-to-use method for quickly setting up project dependencies.

Often - a particular website depends on a wide variety of python libraries.
These include, but are not limited to external open source libraries and a
set of internal closed libraries checked out from various repositories.

To use baste you must already have a working python interpreter with
virtualenv installed. In addition, if your project depends on python libraries
which must be compiled - the necessary non-python dependencies must be
installed external to Baste.  For example, if you use PIL - you must have
the necessary imaging libraries installed before you use baste. If you use
psycopg2 - you must also have the necessary postgres development libraries
installed. If you will be using code checked out from code repositories the
necessary repository command line tools must also be present and available.
In ubuntu - these would be some form of the following::

    sudo apt-get install python mercurial git subversion python-dev gcc g++ python-virtualenv python2.6-dev postgresql-server-dev-9.1

To see an example of how all of this might be setup, see
https://bitbucket.org/lakin.wecker/baste-example/overview  In this example
we use a ``start.sh`` file to setup the virtualenv and install the external
project dependencies.  The ``fabfile.py`` contains the Fabric/Baste
setup. Typically the repositories managed by Baste are ones that may
be developed in parallel with the main project.  In this case, the only
dependency like that is a made up on that simply has a pythonpackage with
a version number. Finally the ``.hgignore`` file setups up some common
ignores.
