# coding=utf-8
# Copyright (c) 2012 Structured Abstraction Inc.
#
# See LICENSE for licensing details.
"""
Baste provides extensions for fabric [1].

Baste allows you to tack together a development stack with simple components in
preparation for development.

[1] - http://docs.fabfile.org/en/1.3.4/index.html

"""
from __future__ import with_statement

import datetime
import os.path
import string
import random

from fabric.api import (
        env,
        execute,
        hide,
        lcd,
        local as local_run,
        run as remote_run,
        settings,
        sudo as remote_sudo,
        prompt
    )
from fabric import colors
from fabric.contrib.project import rsync_project

try:
    from collections import OrderedDict
except ImportError:
    from baste.py24 import OrderedDict

__all__ = [
    'DiffCommand',
    'Git',
    'Mercurial',
    'OrderedDict',
    'PgRestore',
    'PgShell',
    'PgLoadPlain',
    'MysqlLoadPlain',
    'MysqlLoadGzip',
    'project_relative',
    'python_dependency',
    'Repository',
    'RsyncMedia',
    'RsyncDeployment',
    'StatusCommand',
    'Subversion',
]

#-------------------------------------------------------------------------------
class ExecuteOnHosts(object):
    """
    Represents a command to be run on a set of hosts.

    Using this allows us to temporarily override the fabric env.hosts for a
    specific command.
    """
    #---------------------------------------------------------------------------
    def __init__(self, command, hosts):
        self.command = command
        self.hosts = list(hosts)

    #---------------------------------------------------------------------------
    def __call__(self, *args, **kwargs):
        kwargs['hosts'] = self.hosts
        return execute(self.command, *args, **kwargs)

#-------------------------------------------------------------------------------
def local_sudo(command, user):
    """
    Run a shell command on a local host, with superuser privileges.

    `sudo` is identical in every way to fabric's `fabric.operations.sudo`, except
    that it will run the command locally, instead of remotely.
    """
    cmd = """sudo -u %s sh -c "%s" """ % (user, command)
    local_run(cmd)

#-------------------------------------------------------------------------------
class BasteEnvironment(object):
    """
    A simple class used to store baste environment settings.
    """
    #---------------------------------------------------------------------------
    def get_baste_dev_hosts(self):
        """
        A helper which retrieves the environments `baste_dev_hosts` setting.

        Defaults to "localhost".
        """
        return getattr(env, 'baste_dev_hosts', 'localhost')
    baste_dev_hosts = property(get_baste_dev_hosts)

    #---------------------------------------------------------------------------
    def get_baste_env_hosts(self):
        """
        Parses the `baste_dev_hosts` as a set of semi-colon separated host values.
        """
        return self.baste_dev_hosts.split(";")
    baste_env_hosts = property(get_baste_env_hosts)

    #---------------------------------------------------------------------------
    def get_run_command(self):
        """
        Returns a run command that will run against either a local or remote env.

        Uses the fabric env setting `baste_dev_hosts` to determine which it is.
        `baste_dev_hosts` may be set to "localhost" or "host1;host2". The default
        is "localhost".  When set to "localhost", the fabric "local" command
        will be used and commands are run directly, rather than over ssh. If
        `baste_dev_hosts` is anything else, it is interpreted as the env.hosts
        necessary to run the commands over an ssh connection.  Multiple hosts
        can be specified by separating them with semi-colons.

        """
        if self.baste_dev_hosts == "localhost":
            return local_run
        else:
            env.hosts = hosts = self.baste_env_hosts
            return ExecuteOnHosts(remote_run, hosts)
    run = property(get_run_command)

    #---------------------------------------------------------------------------
    def get_sudo_command(self):
        """
        Returns a sudo command that will run against either a local or remote env.

        Uses the fabric env setting `baste_dev_hosts` to determine which it is.
        `baste_dev_hosts` may be set to "localhost" or "host1;host2". The default
        is "localhost".  When set to "localhost", the fabric "local" command
        will be used and commands are run directly, rather than over ssh. If
        `baste_dev_hosts` is anything else, it is interpreted as the env.hosts
        necessary to run the commands over an ssh connection.  Multiple hosts
        can be specified by separating them with semi-colons.
        """
        if self.baste_dev_hosts == "localhost":
            return local_sudo
        else:
            env.hosts = hosts = self.baste_env_hosts
            return ExecuteOnHosts(remote_sudo, hosts)
    sudo = property(get_sudo_command)
baste_env = BasteEnvironment()

#-------------------------------------------------------------------------------
def project_relative(path):
    return os.path.join(os.path.dirname(env.real_fabfile), path)

#-------------------------------------------------------------------------------
class Repository(object):
    """
    Generic base repository which knows how to construct a safe update command.
    """
    #---------------------------------------------------------------------------
    def __init__(self, name, url):
        """Instantiate the repository with the given name and url"""
        self.name = name
        self.directory = project_relative(self.name)
        self.url = url

    #---------------------------------------------------------------------------
    def status(self, capture=False):
        return baste_env.run(self.status_command(), capture)

    #---------------------------------------------------------------------------
    def update(self):
        print(colors.green("[update] ") + self.name)
        with hide('running'):
            cmd = "test -d %s && %s || %s" % (
                        self.directory, self.update_command(), self.create_command()
                        )
            baste_env.run(cmd)

#-------------------------------------------------------------------------------
class RepositoryMismatch(RuntimeError):
    """
    Indicates that the repository that is being updated is of a different type.
    """

#-------------------------------------------------------------------------------
class Subversion(Repository):
    """Represents a subversion repository."""
    #---------------------------------------------------------------------------
    def create_command(self):
        return "svn co %s %s" % (self.url, self.directory)

    #---------------------------------------------------------------------------
    def _ensure_subversion(self):
        dot_svn = os.path.join(self.directory, ".svn")
        if os.path.exists(self.directory) and not os.path.exists(dot_svn):
            raise RepositoryMismatch(
                    ("%s is not an svn checkout. "
                    "Double check you have no uncommitted/unpushed changes "
                    "and then remove this directory and fab up again.") % self.directory
                )

    #---------------------------------------------------------------------------
    def update_command(self):
        self._ensure_subversion()
        return "svn up %s" % (self.directory)

    #---------------------------------------------------------------------------
    def status_command(self):
        self._ensure_subversion()
        return "svn st %s" % (self.directory)

    #---------------------------------------------------------------------------
    def diff_command(self):
        self._ensure_subversion()
        return "svn diff %s" % (self.directory)

#-------------------------------------------------------------------------------
class Mercurial(Repository):
    """Represents a mercurial repository."""

    #---------------------------------------------------------------------------
    def __init__(self, name, url, branch=None):
        self.name = name
        self.directory = project_relative(self.name)
        self.url = url
        self.branch = branch

    #---------------------------------------------------------------------------
    def create_command(self):
        if self.branch is not None:
            return "hg clone %s %s; hg -R %s update '%s'" % (self.url, self.directory, self.directory, self.branch)
        else:
            return "hg clone %s %s" % (self.url, self.directory)

    #---------------------------------------------------------------------------
    def _ensure_mercurial(self):
        dot_hg = os.path.join(self.directory, ".hg")
        if os.path.exists(self.directory) and not os.path.exists(dot_hg):
            raise RepositoryMismatch(
                    ("%s is not an hg clone. "
                    "Double check you have no uncommitted/unpushed changes "
                    "and then remove this directory and fab up again.") % self.directory
                )

    #---------------------------------------------------------------------------
    def update_command(self):
        self._ensure_mercurial()
        if self.branch is not None:
            return "hg -R %s pull; hg -R %s update '%s'" % (self.directory, self.directory, self.branch)
        else:
            return "hg -R %s pull -u" % (self.directory)

    #---------------------------------------------------------------------------
    def status_command(self):
        self._ensure_mercurial()
        return "hg -R %s status" % (self.directory)

    #---------------------------------------------------------------------------
    def diff_command(self):
        self._ensure_mercurial()
        return "hg -R %s diff" % (self.directory)

#-------------------------------------------------------------------------------
class Git(Repository):
    """Represents a mercurial repository."""

    #---------------------------------------------------------------------------
    def __init__(self, name, url, branch=None, tag=None):
        self.name = name
        self.directory = project_relative(self.name)
        self.url = url
        self.branch = branch
        if not tag and not branch:
            self.branch = 'master'
        self.tag = tag
        if self.tag and self.branch:
            raise RuntimeError("Can't specify both a branch and a tag!!")

    #---------------------------------------------------------------------------
    def create_command(self):
        if self.tag:
            return "git clone %s %s && cd %s; git checkout %s" % (self.url, self.directory, self.directory, self.tag)
        else:
            return "git clone %s %s && cd %s; git checkout %s" % (self.url, self.directory, self.directory, self.branch)

    #---------------------------------------------------------------------------
    def update_command(self):
        if self.tag:
            return "cd %s && git checkout %s" % (self.directory, self.tag)
        else:
            return "cd %s && git checkout %s && git fetch origin && git pull origin %s" % (self.directory, self.branch, self.branch)

    #---------------------------------------------------------------------------
    def status_command(self):
        return "cd %s && git status" % (self.directory)

    #---------------------------------------------------------------------------
    def diff_command(self):
        return "cd %s && git diff" % (self.directory)

#-------------------------------------------------------------------------------
def python_dependency(package, python_version, dir=None):
    """Adds the given package as a dependency for the given python version."""

    # Figure out the directory we'll be symlinking to.
    if dir is None:
        base_dir = project_relative(package)
        if os.path.exists(os.path.join(base_dir, "__init__.py")):
            dir = os.path.join(base_dir, '..')
        elif os.path.exists(os.path.join(base_dir, package, "__init__.py")):
            dir = base_dir
        elif os.path.exists(os.path.join(base_dir, 'src', package, "__init__.py")):
            dir = os.path.join(base_dir, 'src')

    from distutils.sysconfig import get_python_lib
    pth_file = "%s/%s.pth" % (get_python_lib(), package)
    pth_file = project_relative(pth_file)
    python_path = dir
    create_pth_file = "echo \"%s\" > %s" % (python_path, pth_file)
    print(colors.green("[install] ") + package)
    with hide('running'):
        baste_env.run("rm %s; %s" % (pth_file, create_pth_file))

#-------------------------------------------------------------------------------
class StatusCommand(object):
    """Helper which prints the status for all of the repos given to it."""

    #---------------------------------------------------------------------------
    def __init__(self, repos):
        self.repos = repos

    #---------------------------------------------------------------------------
    def __call__(self):
        """
        Prints the status for each of the repositories.
        """
        with hide('running'):
            for repo in self.repos.values():
                print(colors.green("[status] ") + repo.name)
                repo.status()

#-------------------------------------------------------------------------------
class DiffCommand(object):
    """Helper which prints the diff for all of the repos given to it."""

    #---------------------------------------------------------------------------
    def __init__(self, repos):
        self.repos = repos

    #---------------------------------------------------------------------------
    def __call__(self):
        """
        Prints the status for each of the repositories.
        """
        commands = []
        for repo in self.repos.values():
            commands.append(repo.diff_command())
        baste_env.run("{ %s; } | less" % (" && ".join(commands)))

#-------------------------------------------------------------------------------
class PgRestore(object):
    #---------------------------------------------------------------------------
    def __init__(self, file, db, user, format="custom"):
        self.file = file
        self.db = db
        self.user = user
        self.format = format

    #---------------------------------------------------------------------------
    def __call__(self):
        """
        Uses the pg_restore command to restore the database from the given file.
        """
        baste_env.run(
            "pg_restore --clean --no-owner --no-privileges --format=%s --host=localhost --username=%s --dbname=%s %s" % (
                self.format, self.user, self.db, self.file
            )
        )

#-------------------------------------------------------------------------------
class PgLoadPlain(object):
    #---------------------------------------------------------------------------
    def __init__(self, file, db, user):
        self.file = file
        self.db = db
        self.user = user

    #---------------------------------------------------------------------------
    def __call__(self):
        """Uses psql to load a plain dump format"""
        baste_env.run(
            "bzcat %s | psql --host=localhost --username=%s --dbname=%s" % (
                self.file, self.user, self.db
            )
        )

#-------------------------------------------------------------------------------
class PgShell(object):
    #---------------------------------------------------------------------------
    def __init__(self, db, user):
        self.db = db
        self.user = user

    #---------------------------------------------------------------------------
    def __call__(self):
        """
        Uses the psql command to give someone a shell within the db.
        """
        baste_env.run("psql --host=localhost --username=%s %s" % (self.user, self.db))

#-------------------------------------------------------------------------------
class MysqlLoadPlain(object):
    #---------------------------------------------------------------------------
    def __init__(self, file, db, user, password=None):
        self.file = file
        self.db = db
        self.user = user
        self.password = password

    #---------------------------------------------------------------------------
    def __call__(self):
        """Uses mysql command line client to load a plain dump format."""
        if not self.password:
            baste_env.run(
                "mysql -h localhost -u %s -p %s < %s" % (self.user, self.db, self.file)
            )
        else:
            baste_env.run(
                "mysql -h localhost -u %s -p%s %s < %s" % (self.user, self.password, self.db, self.file)
            )

#-------------------------------------------------------------------------------
class MysqlLoadGzip(object):
    #---------------------------------------------------------------------------
    def __init__(self, file, db, user, password=None):
        self.file = file
        self.db = db
        self.user = user
        self.password = password

    #---------------------------------------------------------------------------
    def __call__(self):
        """Uses mysql command line client to load a plain dump format."""
        if not self.password:
            baste_env.run(
                "gunzip --stdout %s | mysql -h localhost -u %s -p %s" % (self.file, self.user, self.db)
            )
        else:
            baste_env.run(
                "gunzip --stdout %s | mysql -h localhost -u %s -p%s %s" % (self.file, self.user, self.password, self.db)
            )

#-------------------------------------------------------------------------------
class MysqlLoadBz2(object):
    def __init__(self, file, db, user, password=None):
        self.file = file
        self.db = db
        self.user = user
        self.password = password

    #---------------------------------------------------------------------------
    def __call__(self):
        """Uses psql to load a plain dump format"""
        if not self.password:
            baste_env.run(
                "bzcat %s | mysql -h localhost -u %s -p %s" % (self.file, self.user, self.db)
            )
        else:
            baste_env.run(
                "bzcat %s | mysql -h localhost -u %s -p%s %s" % (self.file, self.user, self.password, self.db)
            )

#-------------------------------------------------------------------------------
class RsyncMedia(object):
    #---------------------------------------------------------------------------
    def __init__(self, host, remote_directory, local_directory):
        self.host = host
        self.local_directory = local_directory
        self.remote_directory = remote_directory

    #---------------------------------------------------------------------------
    def __call__(self):
        """
        Uses the psql command to give someone a shell within the db.
        """
        local_run("rsync --progress -avz --exclude=\".svn/*\" --exclude=\".svn\" -e ssh %s:%s %s" % (self.host, self.remote_directory, self.local_directory))

REPO_EXCLUDES = [
    '.svn', '.git', '.hg', '.hgignore', 'requirements.txt', 'start.sh',
    'fabfile.py', '*.pyc', 'env', '*.swp', '.venv', 'env'
]

#-------------------------------------------------------------------------------
class RsyncDeployment(object):
    """
    An rsync deployment consists of the following steps:
        1. the local_directory is rsync'ed to the remote_directory/source
        2. an cp command will be run that cps remote_directory/source to
           remote_directory/<current_date_time>
        3. a remote command is run to symlink remote_directory/current to
           remote_directory/<current_date_time>
        4. It is then up to the caller to run the command to reload the server

    Please note that this command WILL delete files on the remote_director/source
    """
    #---------------------------------------------------------------------------
    def __init__(self, remote_directory, local_directory):
        self.remote_directory = remote_directory
        self.local_directory = local_directory

    #---------------------------------------------------------------------------
    def __call__(self, exclude=[]):
        """
        Actually perform the deployment.
        """

        exclude += REPO_EXCLUDES

        source_directory = os.path.join(self.remote_directory, "source")
        previous_directory = os.path.join(self.remote_directory, "previous")
        tmp_previous = os.path.join(self.remote_directory, "previous_tmp")
        current_symlink = os.path.join(self.remote_directory, "current")
        tmp_symlink = os.path.join(self.remote_directory, "current_tmp")
        date_directory = os.path.join(
                self.remote_directory,
                datetime.datetime.now().strftime("%Y-%m-%dT%H%M%S")
            )
        remote_run("mkdir -p %s" % source_directory)
        remote_run("mkdir -p %s" % previous_directory)
        remote_run("rm -rf %s" % (tmp_previous,))
        remote_run("mv -Tf %s %s" % (previous_directory, tmp_previous))

        # only if the current directly exists can we copy it to the previous directory.
        if remote_run("ls -1 %s | grep current | cat" % self.remote_directory):
            remote_run("cp -rH %s %s" % (current_symlink, previous_directory))

        rsync_project(
                remote_dir=source_directory,
                local_dir=self.local_directory,
                exclude=exclude,
                delete=True,
            )
        remote_run("cp -r %s %s" % (source_directory, date_directory))
        remote_run("ln -sf %s %s; mv -Tf %s %s" % (date_directory, tmp_symlink, tmp_symlink, current_symlink))
        remote_run("rm -rf %s" % tmp_previous)

#-------------------------------------------------------------------------------
class UbuntuPgCreateDbAndUser(object):
    #---------------------------------------------------------------------------
    def __init__(
        self,
        db_name,
        db_user,
        source_template=None,
        create_role=True,
        as_user='postgres'
    ):
        self.db_name = db_name
        self.db_user = db_user
        self.source_template = source_template
        self.as_user = as_user
        self.create_role = create_role

    #---------------------------------------------------------------------------
    def __call__(self):
        if not self.as_user:
            run = lambda c: baste_env.run(c)
        else:
            run = lambda c: baste_env.sudo(c, user=self.as_user)

        with settings(warn_only=True, hide=('warnings',)):
            run("dropdb {}".format(self.db_name))

            if self.create_role:
                run("dropuser {}".format(self.db_user))
                run("createuser --createdb --pwprompt --no-superuser --no-createrole {}".format(self.db_user))
            if not self.source_template:
                run("createdb -O {} {}".format(self.db_user, self.db_name))
            else:
                run("createdb -T {} -O {} {}".format(self.source_template, self.db_user, self.db_name))

#-------------------------------------------------------------------------------
class UbuntuMysqlCreateDbAndUser(object):
    #---------------------------------------------------------------------------
    def __init__(self, db_name, db_user, db_password):
        self.db_name = db_name
        self.db_user = db_user
        self.db_password = db_password

    #---------------------------------------------------------------------------
    def __call__(self):
        # Drop the db
        sql = "DROP DATABASE IF EXISTS %(db)s; "
        sql = sql % { 'db': self.db_name, }
        command = "echo \"%s\" | mysql -p -u root" % (sql,)
        baste_env.run(command)

        # Re create it.
        sql = "CREATE DATABASE %(db)s; " + \
            "GRANT ALL ON %(db)s.* to %(user)s@localhost IDENTIFIED BY '%(pw)s';"
        sql = sql % {
                'db': self.db_name,
                'user': self.db_user,
                'pw': self.db_password,
            }

        command = "echo \"%s\" | mysql -p -u root" % (sql,)
        baste_env.run(command)


#-------------------------------------------------------------------------------
def pay_attention_confirm(question, yes=None):
    """Ask a user a question, require a unique answer for confirmation."""

    if yes is None:
        yes = ''.join([random.choice(string.ascii_letters) for x in range(3)])
        yes = yes.lower()

    response = prompt('%s [ y("%s") / n ]:' % (question, yes))
    if response == yes:
        return True
    return False


#-------------------------------------------------------------------------------
def local_changes(repos):
    changes = []
    for repo in repos:
        new_changes = repo.status(capture=True)
        if isinstance(repo, Git):
            ignored_lines = [
                'nothing to commit (working directory clean)',
                'nothing to commit, working directory clean',
                '# On branch master\n',
                'nothing to commit, working tree clean',
                'On branch master',
                'Your branch is up to date with \'origin/master\'.',
            ]
            for line in ignored_lines:
                new_changes = new_changes.replace(line, '')
            new_changes = "\n".join([
                line for line in new_changes.split("\n")
                if not line.startswith("HEAD detached at ")
            ])

        # Do the funky svn parsing dance. ;)
        if isinstance(repo, Subversion):
            updated_changes = []
            for line in new_changes.split('\n'):
                stripped = line.strip()

                # skip random lines about externals
                if stripped.startswith("Performing status on external item at '"):
                    continue
                # skip lines listing externals.
                parts = [part.strip() for part in stripped.split(' ') if part]
                if len(parts) > 0 and parts[0] == "X":
                    continue

                updated_changes.append(line)
            new_changes = '\n'.join(updated_changes)
        new_changes = new_changes.strip()
        if new_changes:
            changes.append(new_changes)

    return changes

