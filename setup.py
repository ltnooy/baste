#!/usr/bin/env python

from setuptools import setup

setup(name='baste',
    version='1.0',
    description='Baste provides extensions for Fabric',
    author='Lakin Wecker',
    author_email='lakin@structuredabstraction.com',
    url='https://bitbucket.org/strabs/baste',
    # setup_requires=['setuptools_scm'],
    # use_scm_version=True,
    # packages=find_packages()
)


